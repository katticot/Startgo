package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"os"
	"strconv"
)

var (
	host     = os.Getenv("POSTGRES_HOST")
	port, _     = strconv.ParseInt(os.Getenv("POSTGRES_PORT"),10,64)
	user     = os.Getenv("POSTGRES_USER")
	password = os.Getenv("POSTGRES_PASSWORD")
	dbname   = os.Getenv("POSTGRES_DBNAME")
	serverPort=os.Getenv("PORT")
)
//POSTGRES_HOST=localhost;POSTGRES_PORT=5432;POSTGRES_PASSWORD=mysecretpassword;POSTGRES_DBNAME=postgres;POSTGRES_USER=postgres;PORT=9193
//var (
//	host     = "localhost"
//	port     = 5432
//	user     = "postgres"
//	password = "mysecretpassword"
//	dbname   = "postgres"
//)

//DBNAME=postgres PASSWORD=mysecretpassword USERDB=postgres PORT=5432 HOST=localhost


func getConstructions(w http.ResponseWriter, request *http.Request) {
	fmt.Printf("host=%s port=%d user=%s password=%s dbname=%s\n",host,port,user,password,dbname)
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		panic(err)

	}

	rows, err := db.Query("select  construction_sites.created_at,construction_sites.updated_at,status,first_name as prenom,last_name as nom, address,name as société from construction_sites,companies where company_id in (3,4,5,6,7,8) and construction_sites.company_id=companies.id LIMIT $1", 50)
	if err != nil {
		// handle this error better than this
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {

		var dtCreation,dtUpdate,status, firstName,lastName,address, company string
		//var valid bool
		err = rows.Scan(&dtCreation, &dtUpdate, &status, &firstName, &lastName, &address, &company)
		if err != nil {
			// handle this error
			panic(err)
		}
		_, _ = fmt.Fprintln(w,dtCreation, dtUpdate, status, firstName, lastName, address, company)
		fmt.Println(dtCreation, dtUpdate, status, firstName, lastName, address, company)
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		panic(err)
	}



}

func handler(w http.ResponseWriter, request *http.Request) {
	_, _ = fmt.Fprintf(w, "Error 404 !!")

}

func main() {
	fmt.Printf("host=%s port=%d user=%s password=%s dbname=%s\n",host,port,user,password,dbname)
	http.HandleFunc("/", handler)
	http.HandleFunc("/constructions", getConstructions)
	fmt.Println("listening on "+serverPort)
	go log.Fatal(http.ListenAndServe(":"+serverPort, nil))
}
