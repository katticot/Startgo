package example

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

func getDevice(body []byte) (*identity, error) {
	var s = new(identity)
	err := json.Unmarshal(body, &s)
	if err != nil {
		fmt.Println("whoops:", err)
	}
	return s, err
}

const (
	// DefaultBaseURL is netatmo api url
	baseURL = "https://api.netatmo.com/"
	// DefaultAuthURL is netatmo auth url
	authURL = baseURL + "oauth2/token"
	// DefaultDeviceURL is netatmo device url
	deviceURL = baseURL + "/api/getstationsdata"
	// DefaultGetDeviceURL is netatmo getDevice url
	getDeviceURL = baseURL + "/api/partnerdevices"
)

//identifiants
const (
	clientId     = "57432a11aaa0dd8a3ac6074f"
	clientSecret = "B4tiPXTf1tuRFi294thioENS8tYWIZWfim6L6U"
	username     = "dc-dn-panel@edf.fr"
	password     = "dnpanel@123"
)

type identity struct {
	Access_token  string `json:"access_token"`
	Refresh_token string `json:"refresh_token"`
	Read_station  string `json:"read_station"`
	expires_in    int64  `json:"expire_in"`
}

func main() {
	// création des paramètre
	params := url.Values{}
	params.Add("client_id", clientId)
	params.Add("client_secret", clientSecret)
	params.Add("usernamesmtp", username)
	params.Add("passwordsmtp", password)
	params.Add("grant_type", "passwordsmtp")

	//// Create request object.
	req, err := http.NewRequest("POST", authURL, strings.NewReader(params.Encode()))
	// Set content type
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("User-Agent", "Netatmo microservice/0.1")
	// Set the header in the request.
	//req.Header.Set("Bearer", "token")
	//
	// Execute the request.
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err.Error())
	}

	s, err := getDevice([]byte(body))

	println("Access_token : " + s.Access_token)
	println("\n")
	println("Refresh_token : " + s.Refresh_token)
	println("\n")
	println("expires_in : " + s.Read_station)

}
