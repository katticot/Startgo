package example

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

//TODO create several  named return variable
func main() {
	const (
		// DefaultBaseURL is netatmo api url
		baseURL = "https://api.netatmo.com/"
		// DefaultAuthURL is netatmo auth url
		authURL = baseURL + "oauth2/token"
		// DefaultDeviceURL is netatmo device url
		deviceURL = baseURL + "/api/getstationsdata"
		// DefaultGetDeviceURL is netatmo getDevice url
		getDeviceURL = baseURL + "/api/partnerdevices"
	)
	//identifiants
	const (
		clientId     = "57432a11aaa0dd8a3ac6074f"
		clientSecret = "B4tiPXTf1tuRFi294thioENS8tYWIZWfim6L6U"
		username     = "dc-dn-panel@edf.fr"
		password     = "dnpanel@123"
	)

	type identity struct {
		access_token  string `json:"access_token"`
		refresh_token string `json:"refresh_token"`
		read_station  string `json:"read_station"`
		expires_in    string `json:"expires_in"`
	}
	params := url.Values{}
	params.Add("client_id", clientId)
	params.Add("client_secret", clientSecret)
	params.Add("usernamesmtp", username)
	params.Add("passwordsmtp", password)
	params.Add("grant_type", "passwordsmtp")

	//// Create request object.
	req, err := http.NewRequest("POST", authURL, strings.NewReader(params.Encode()))
	// Set content type
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("User-Agent", "Netatmo microservice/0.1")
	// Set the header in the request.
	//req.Header.Set("Bearer", "token")
	//
	// Execute the request.
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	//Parsing du body
	body := resp.Body
	body2 := resp.Body
	Decodebody, err := ioutil.ReadAll(body)
	identity1 := new(identity)
	_ = json.NewDecoder(body2).Decode(identity1)

	fmt.Print("Status: " + resp.Status)
	fmt.Print("\n")

	fmt.Printf("%+v\n", identity1)
	fmt.Print("body: " + identity1.refresh_token)
	fmt.Print("body: " + identity1.access_token)
	fmt.Print("body: " + identity1.read_station)
	fmt.Print("full body: " + string(Decodebody))

}
