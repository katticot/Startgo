package example

import "fmt"

func main() {
	d := make(chan string)
	var dodo int
	fmt.Printf("value of c is " + <-d + "\n")

	d <- "hey"

	fmt.Printf("type of c is " + <-d + "\n")
	fmt.Printf("type of c is %T\n", <-d)
	fmt.Printf("value of c is %v", dodo)

}
