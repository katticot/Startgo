package example

import (
	"fmt"
	"net/http"
)

func main() {
	var city string
	appid := "appid=b6907d289e10d714a6e88b30761fae22"
	city = "Paris"
	resp, err := http.Get("https://samples.openweathermap.org/data/2.5/forecast?q=" + city + "&appid=" + appid)

	fmt.Print(resp.Status + "\n")
	fmt.Print(err)
}
