FROM golang:alpine as builder
RUN mkdir /build
RUN apk add git
ADD . /build/
WORKDIR /build
RUN go get github.com/lib/pq && go build -o main .
FROM alpine
RUN adduser -S -D -H -h /app appuser
USER appuser
COPY --from=builder /build/main /app/
WORKDIR /app
CMD ["./main"]
